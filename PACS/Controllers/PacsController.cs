﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NLog;
using System;

namespace PACS.Controllers
{
    [Route("pacscall")]
    [ApiController]
    public class PacsController : ControllerBase
    {
        private readonly IConfiguration _setting;
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();


        public PacsController(IConfiguration settings)
        {
            _setting = settings;

            var config = new NLog.Config.LoggingConfiguration();

            var file = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs\\PacsCall-{DateTime.Now:MM-dd-yyyy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, file);
            LogManager.Configuration = config;
        }

        public ActionResult Get(string id)
        {
            Logger.Info("Received new id for pacscall passthrough.");
            var strUrl = "";

            //Grab id that is passed to application
            try
            {
                var strId = id.Trim();

                try
                {
                    var length = strId.Length;
                    strId = strId.Substring(2,length-2);
                    strUrl = _setting["AppSettings:RedirectUrl"] + "&ris_pat_id=" + strId;
                }
                catch (Exception e)
                {
                    Logger.Error("There was an issue getting the passthrough URL. :: Exception: " + e.Message);
                }
            }
            catch (Exception e)
            {
                Logger.Error("There was an issue handling the received id. :: Exception: " + e.Message);
            }

            return Redirect(strUrl);
        }
    }
}
