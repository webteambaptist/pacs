﻿using Microsoft.AspNetCore.Mvc;

namespace PACS.Controllers
{
    [Route("home")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            var message = "Starting Pacscall...";
            return message;
        }
    }
}
